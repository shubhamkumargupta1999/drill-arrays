function reduce(elements, cb, startingValue) {

    for (let index = 0; index < elements.length; index++) {

        if (index === 0 && startingValue === undefined) {
            startingValue = elements[index];
            index++;
        }

        startingValue = cb(startingValue, elements[index], index, elements);
    }

    return startingValue;
}

module.exports = reduce;