function map(elements, cb) {

    if (arguments.length < 2 || !Array.isArray(elements)) {
        return [];
    }

    let arr = []

    for (let index = 0; index < elements.length; index++) {

        arr.push(cb(elements[index], index, elements));

    }

    return arr;
}

module.exports = map;