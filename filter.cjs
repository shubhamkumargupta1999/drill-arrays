function filter(elements, cb) {

    if (arguments < 2 || !Array.isArray(elements)) {
        return [];
    }

    const arr = []

    for (let index = 0; index < elements.length; index++) {

        if (cb(elements[index], index, elements) === true) {
            arr.push(elements[index]);
        }
    }

    return arr;
}

module.exports = filter;