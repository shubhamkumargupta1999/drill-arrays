function flatten(elements, depth = 1) {
  const arr = [];

  function flattenhelper(elements, currentDepth) {

    for (const element of elements) {

      if (Array.isArray(element) && currentDepth < depth) {
        
        flattenhelper(element, currentDepth + 1);
      } 
      
      else if (element !== undefined) {
        arr.push(element);
      }
    }
  }

  flattenhelper(elements, 0);
  return arr;
}

module.exports = flatten;