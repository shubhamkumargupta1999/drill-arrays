const items = require('../items.cjs');
const reduce = require('../reduce.cjs');

function cb(accumulator, item) {
    return accumulator * item;
}

test("testing reduce", () => {
    expect(reduce(items, cb)).toBe(items.reduce(cb));
})
