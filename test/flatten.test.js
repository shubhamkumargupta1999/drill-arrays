const nestedArray = require('../nestedArray.cjs');
const flatten = require('../flatten.cjs');

test("testing flatten", () => {
    expect(flatten(nestedArray, 2)).toStrictEqual(nestedArray.flat(2))
})
