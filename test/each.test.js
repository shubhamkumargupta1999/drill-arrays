const items = require('../items.cjs');
const each = require('../each.cjs');

function cb() {
}


test('testing each', () => {
    expect(each(items, cb)).toStrictEqual(items.forEach((1, cb)))
})
