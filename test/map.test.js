const items = require('../items.cjs');
const map = require('../map.cjs');

function cb(item) {
    return item * 2;
}

test("testing map", () => {
    expect(map(items, cb)).toStrictEqual(items.map(cb));
})
