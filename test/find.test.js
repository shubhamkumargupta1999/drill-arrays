const items = require('../items.cjs');
const find = require('../find.cjs');

function cb(item) {
    return (item * 2 === 4);
}


test("testing find", () => {
    expect(find(items, cb)).toStrictEqual(items.find(cb))
})
