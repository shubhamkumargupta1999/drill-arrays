const items = require('../items.cjs');
const filter = require('../filter.cjs');

function cb(item) {

    return (item%2 == 0);
}

test('testing filter', () => {
    expect(filter(items, cb)).toStrictEqual(items.filter(cb))
})
