function each(elements, cb) {

    if (arguments < 2 || !Array.isArray(elements)) {
        return undefined;
    }

    let arr = [];

    for (let index = 0; index < elements.length; index++) {
        arr.push(cb());
    }

}

module.exports = each;
